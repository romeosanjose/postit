<?php


namespace App\Http\Controllers;


use App\Core\Domain\Boundery\Request;
use App\Core\Setting\Repository\ManageListRepository;
use App\Core\Setting\UseCase\GetManageList;
use App\Core\User\Repository\UserRepository;
use App\Core\User\User;

class ManageListController extends Controller
{
    private $userEntity;
    private $userRepository;
    private $managelistRepository;

    public function __construct(UserRepository $userRepository,
                                ManageListRepository $manageListRepository,
                                User $user)
    {
        $this->userEntity = $user;
        $this->userRepository = $userRepository;
        $this->managelistRepository = $manageListRepository;
    }

    public function getAllAction($module)
    {
        try{
            $manageUseCase = new GetManageList($this->managelistRepository);
            $response = $manageUseCase->getManageLists($module);

            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }
}
