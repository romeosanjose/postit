<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 23/08/2019
 * Time: 1:47 PM
 */

namespace App\Http\Controllers;

use App\Core\Post\Decorator\PatientDecorator;
use App\Core\Post\Patient;
use App\Core\Post\Repository\PatientRepository;
use App\Core\Domain\Boundery\Request as UseCaseRequest;
use App\Core\Post\UseCase\AddPatient;
use App\Core\Post\UseCase\GetPatient;
use App\Core\Domain\Validator\FormValidationException;
use App\Core\User\Repository\UserRepository;
use App\Core\User\User;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    private $patientRepository;
    private $userRepository;

    public function __construct(PatientRepository $patientRepository, UserRepository $userRepository)
    {
        $this->patientRepository = $patientRepository;
        $this->userRepository = $userRepository;
    }

    public function storeAction(Request $request)
    {
        $createdBy = $this->userRepository->findBy(['api_key' => $request->header('api-key')])[0];
        $patientRequest = new UseCaseRequest($request->json('patient'));
        try {
            $response = (new AddPatient($this->patientRepository))->addNewRecord(
                $patientRequest, new Patient(), $createdBy, new PatientDecorator());
            return response($response->getData(), $response->getCode());
        }catch(FormValidationException $e){
            return response($e->getErrors(), 500);
        }
    }

    public function getAllAction()
    {
        try{
            $response = (new GetPatient($this->patientRepository))->getAllActiveRecords(new PatientDecorator());
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function getPatientAction($id)
    {
        try{
            $response = (new GetPatient($this->patientRepository))->getPatient($id, new PatientDecorator());
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }
}
