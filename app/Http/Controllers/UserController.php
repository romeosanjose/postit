<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 14/06/2019
 * Time: 9:43 AM
 */

namespace App\Http\Controllers;


use App\Core\File\Decorator\FileDecorator;
use App\Core\Domain\Boundery\Request as UseCaseRequest;
use App\Core\File\File;
use App\Core\Domain\Repository\CannotPersistException;
use App\Core\File\Repository\FileRepository;
use App\Core\File\UseCase\AddFile;
use App\Core\File\UseCase\GetFile;
use App\Core\Domain\Validator\FormValidationException;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Repository\TemplateRepository;
use App\Core\Setting\Repository\ManageListRepository;
use App\Core\Setting\UseCase\GetManageList;
use App\Core\User\Decorator\PersonDecorator;
use App\Core\User\Decorator\UserProtectedDecorator;
use App\Core\User\Events\UserCreated;
use App\Core\User\Person;
use App\Core\User\Repository\PersonRepository;
use App\Core\User\Repository\UserRepository;
use App\Core\User\UseCase\AddPerson;
use App\Core\User\UseCase\AddUser;
use App\Core\User\UseCase\FollowUser;
use App\Core\User\UseCase\GetPerson;
use App\Core\User\UseCase\GetUser;
use App\Core\User\UseCase\UnlockUser;
use App\Core\User\UseCase\UpdatePerson;
use App\Core\User\UseCase\UpdateProfilePicture;
use App\Core\User\UseCase\UpdateUser;
use App\Core\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{
    private $userEntity;
    private $userRepository;
    private $personRepository;
    private $fileRepository;
    private $templateRepository;
    private $eventRepository;
    private $manageListRepository;

    public function __construct(UserRepository $userRepository,
                                PersonRepository $personRepository,
                                FileRepository $fileRepository,
                                TemplateRepository $templateRepository,
                                NotificationEventRepository $eventRepository,
                                ManageListRepository $manageListRepository,
                                User $user)
    {
        $this->userEntity = $user;
        $this->userRepository = $userRepository;
        $this->personRepository = $personRepository;
        $this->fileRepository = $fileRepository;
        $this->templateRepository = $templateRepository;
        $this->eventRepository = $eventRepository;
        $this->manageListRepository = $manageListRepository;
    }

    public function storeAction(Request $request)
    {
        try {
            $userReq = new UseCaseRequest($request->json('user'));
            $personReq = new UseCaseRequest($userReq->get('person'));
            $createdBy = $this->userRepository->findBy(['api_key' => $request->header('api-key')])[0];
            $file = ($personReq->has('file') && !is_null($personReq->get('file')))
                ? (new GetFile($this->fileRepository))->getRecord($personReq->get('file')['uuid'])
                : null;
            $person = $this->addPersonRecord($personReq, $file, $createdBy);
            $userData = $userReq->getData();
            $userData['person'] = $person;
            $userReq->setData($userData);
            $response = (new AddUser($this->userRepository))->addUser($userReq, $createdBy, $this->eventRepository, $this->templateRepository);

            return response($response->getData(), $response->getCode());
        }catch(FormValidationException $formE){
            return response($formE->getErrors(), 500);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }

    public function getAllAction()
    {
        try{
            $useCase = new GetUser($this->userRepository);
            $response = $useCase->getUsers();
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }
    public function getUser($id)
    {
        try{
            $useCase = new GetUser($this->userRepository);
            $response = $useCase->getRecord($id, new UserProtectedDecorator());
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function updateAction(Request $request, $id)
    {
        try{
            $updatedBy = $this->userRepository->findBy(['api_key' => $request->header('api-key')])[0];
            $userReq = new UseCaseRequest($request->json('user'));
            $personReq = new UseCaseRequest($userReq->get('person'));
            $file = ($personReq->has('file') && !is_null($personReq->get('file')))
                ? (new GetFile($this->fileRepository))->getRecord($personReq->get('file')['uuid'])
                : null;
            $person = $this->updatePersonRecord($personReq, $file, $updatedBy);
            if (!(new UpdateUser($this->userRepository))->updateUser($userReq, $person, $updatedBy)) {
                return response('Not able to update record', 501);
            }
            return response( 'Successfully Updated!' , 200);
        }catch(FormValidationException $formE){
            return response($formE->getErrors(), 500);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }

    public function unlockAction($id)
    {
        try{
            $useCase = new UnlockUser($this->userRepository);
            $response = $useCase->unlockAccount($id);
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function followAction($id, Request $request)
    {
        try{
            $userToFollow = $request->json('user');
            $currentUser = (new GetUser($this->userRepository))->getRecord($id);
            if ((new FollowUser($this->userRepository))->follow($userToFollow, $currentUser)){
                return response( 'Successfully Followed!' , 200);
            }

            return response('Not able to follow user', 501);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }

    public function unFollowAction($id, Request $request)
    {
        try{
            $followedUser = $request->json('user');
            $currentUser = (new GetUser($this->userRepository))->getRecord($id);
            if ((new FollowUser($this->userRepository))->unfollow($followedUser, $currentUser)){
                return response( 'Successfully un-followed!' , 200);
            }

            return response('Not able to follow user', 501);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }

    private function updateFileRecord(UseCaseRequest $personReq, User $user)
    {
        $fileReq = new UseCaseRequest($personReq->get('file'));
        $file = (new GetFile($this->fileRepository))->getRecord($fileReq->get('id'));
        if ($file) {
            if ((new UpdateProfilePicture($this->fileRepository))->updateRecord($fileReq, $user)) {
                $file = (new GetFile($this->fileRepository))->getRecord($fileReq->get('id'));
            }
        } else {
            $file = $this->addFileRecord($fileReq, $user);
        }

        return $file;
    }

    private function updatePersonRecord(UseCaseRequest $personReq, $file, User $user)
    {
        $personReq = $this->setPersonSettings($personReq);
        $person = (new GetPerson($this->personRepository))->getRecord($personReq->get('id'));

        if ((new UpdatePerson($this->personRepository))->updatePerson($personReq, $file, $user)) {
            $person = (new GetPerson($this->personRepository))->getRecord($personReq->get('id'));
        }

        return $person;
    }

    private function addFileRecord(UseCaseRequest $fileReq, User $user)
    {
        return (new AddFile($this->fileRepository))->addNewRecord($fileReq, new File(), $user);
    }

    private function addPersonRecord(UseCaseRequest $personReq , $file, User $user)
    {
        $personReq = $this->setPersonFile($personReq, $file);
        $personReq = $this->setPersonSettings($personReq);

        return (new AddPerson($this->personRepository))->addNewRecord($personReq, new Person(), $user);
    }

    private function setPersonFile(UseCaseRequest $personReq, $file)
    {
        if ($file && !is_null($file)){
            $personData = $personReq->getData();
            $personData['profile_picture'] = $file;
            unset($personData['file']);
            $personReq->setData($personData);
        }

        return $personReq;
    }

    private function setPersonSettings(UseCaseRequest $personReq)
    {
        $personData = $personReq->getData();
        $personData['shift_color'] = (new GetManageList($this->manageListRepository))->getRecord($personData['shift_color']);
        $personData['clinical_department'] = (new GetManageList($this->manageListRepository))->getRecord($personData['clinical_department']);
        $personData['year_level'] = (new GetManageList($this->manageListRepository))->getRecord($personData['year_level']);
        $personReq->setData($personData);

        return $personReq;
    }
}
