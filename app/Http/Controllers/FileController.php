<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 19/09/2019
 * Time: 2:02 PM
 */

namespace App\Http\Controllers;

use App\Core\File\Decorator\FileDecorator;
use App\Core\File\Repository\FileRepository;
use App\Core\File\UseCase\AddFile;
use App\Core\File\UseCase\GetFile;
use App\Core\User\Repository\UserRepository;
use Illuminate\Http\Request;
use App\Core\Domain\Boundery\Request as UseCase;


class FileController extends Controller
{
    private $fileRepository;
    private $userRepository;

    public function __construct(FileRepository $fileRepository, UserRepository $userRepository)
    {
        $this->fileRepository = $fileRepository;
        $this->userRepository = $userRepository;
    }

    public function getAction($id)
    {
        try{
            $response =  (new GetFile($this->fileRepository))->getRecord($id, new FileDecorator());
            return $this->setResponse($response);
        } catch (\Exception $e) {
            return response($e->getTraceAsString(), 500);
        }
    }

    public function storeAction(Request $request)
    {
        try {
            $createdby = $this->userRepository->findBy(['api_key' => $request->header('api-key')])[0];
            $fileResponse = (new AddFile($this->fileRepository))->addFile(new UseCase($request->file('file')),$createdby);
            return response($fileResponse->getData(), $fileResponse->getCode());
        } catch (\Exception $e) {
            return response($e->getTraceAsString(), 500);
        }
    }
}
