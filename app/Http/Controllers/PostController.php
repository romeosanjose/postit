<?php


namespace App\Http\Controllers;


use App\Core\Domain\Boundery\Request as UseCaseRequest;
use App\Core\Domain\Repository\CannotPersistException;
use App\Core\Post\Repository\PatientRepository;
use App\Core\Post\Repository\PostRepository;
use App\Core\Post\UseCase\AcceptPost;
use App\Core\Post\UseCase\AddPatient;
use App\Core\Post\UseCase\AddPost;
use App\Core\Post\UseCase\GetPatient;
use App\Core\Post\UseCase\GetPost;
use App\Core\Post\UseCase\UpdatePatient;
use App\Core\Domain\Validator\FormValidationException;
use App\Core\Post\UseCase\UpdatePost;
use App\Core\User\Repository\UserRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    private $postRepo;
    private $patientRepo;
    private $userRepo;

    public function __construct(
        PostRepository $postRepo,
        PatientRepository $patientRepo,
        UserRepository $userRepo
    )
    {
        $this->patientRepo = $patientRepo;
        $this->postRepo = $postRepo;
        $this->userRepo = $userRepo;
    }

    public function storeAction(Request $request)
    {
        $postRequest = new UseCaseRequest($request->json('post'));
        try {
            $createdBy = $this->userRepo->findBy(['api_key' => $request->header('api-key')])[0];
            $response = (new AddPost($this->postRepo))->addPost($postRequest, $this->patientRepo, $createdBy);
            return response($response->getData(), $response->getCode());
        }catch(FormValidationException $e){
            return response($e->getErrors(), 500);
        }
    }

    public function getAllPostAction()
    {
        try{
            $response = (new GetPost($this->postRepo))->getPosts();
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function getAllPostByTreatmentAction($treatment)
    {
        try{
            $response = (new GetPost($this->postRepo))->getPostByTreatment($treatment);
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function getPostAction($id)
    {
        try{
            $response = (new GetPost($this->postRepo))->getPost($id);
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function acceptPostAction($id, Request $request)
    {
        try{
            $acceptedBy = $this->userRepo->findBy(['api_key' => $request->header('api-key')])[0];
            $acceptPostUseCase = new AcceptPost($this->postRepo);
            $response = $acceptPostUseCase->acceptPost($id, $acceptedBy);
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function updatePost($id, Request $request)
    {
        try{
            $postRequest = new UseCaseRequest($request->json('post'));
            $updatedBy = $this->userRepo->findBy(['api_key' => $request->header('api-key')])[0];
            if ((new UpdatePost($this->postRepo))->updatePost($id, $postRequest, $this->patientRepo, $updatedBy)){
                return response('Successfully Updated record', 200);
            }
            return response('Unable to update record', 501);
        }catch(FormValidationException $formE){
            return response($formE->getErrors(), 500);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }

}
