<?php


namespace App\Http\Controllers;


use App\Core\Domain\Repository\CannotPersistException;
use App\Core\Domain\Validator\FormValidationException;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Repository\TemplateRepository;
use App\Core\User\Repository\UserRepository;
use App\Core\User\UseCase\ChangePassword;
use App\Core\User\UseCase\GeneratePassword;
use App\Core\User\UseCase\SendPasswordLink;
use Hackzilla\PasswordGenerator\Generator\HumanPasswordGenerator;
use Illuminate\Http\Request;
use App\Core\Domain\Boundery\Request as UCRequest;

class PasswordController extends Controller
{
    private $userRepository;
    private $eventRepository;
    private $templateRepository;

    public function __construct(
        UserRepository $userRepository,
        NotificationEventRepository $notificationEventRepository,
        TemplateRepository $templateRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->eventRepository = $notificationEventRepository;
        $this->templateRepository = $templateRepository;
    }

    public function generatePassword()
    {
        $generatePasswordCommand = new GeneratePassword(new HumanPasswordGenerator());
        return $generatePasswordCommand->generatePassword();
    }

    public function changePassword($uuid, Request $request)
    {
        try{
            $changePassword = new ChangePassword($this->userRepository);
            $password = $request->json('password');
            $userReq = new UCRequest($request->json('password'));
            $response = $changePassword->changePassword($userReq, $uuid);
            return response($response->getData(), $response->getCode());
        }catch(FormValidationException $formE){
            return response($formE->getErrors(), 500);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }

    public function sendPasswordLink(Request $request)
    {
        try{
            $sendPasswordLink = new SendPasswordLink($this->userRepository);
            $email = $request->json('email');
            $sendPasswordLink->getLinkFromEmail($email, $this->eventRepository, $this->templateRepository);
            return response($email, 200);
        }catch(FormValidationException $formE){
            return response($formE->getErrors(), 500);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }
}
