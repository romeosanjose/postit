<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 05/09/2019
 * Time: 10:52 AM
 */

namespace App\Http\Controllers;


use App\Core\User\User;
use App\Core\User\Repository\UserRepository;
use App\Core\User\UseCase\LoginUser;
use Illuminate\Http\Request;
use App\Core\Domain\Boundery\Request as UseCaseRequest;

class LoginController extends Controller
{
    private $userEntity;
    private $userRepository;

    public function __construct(UserRepository $userRepository,
                                User $user)
    {
        $this->userEntity = $user;
        $this->userRepository = $userRepository;
    }

    public function loginAction(Request $request)
    {
        try{
            $useCaseRequest = new UseCaseRequest($request->json('login'));
            $useCase = new LoginUser($this->userRepository);
            $response = $useCase->doLogin($useCaseRequest);
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }



}