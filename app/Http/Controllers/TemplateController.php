<?php


namespace App\Http\Controllers;


use App\Core\Domain\Boundery\Request as UseCaseRequest;
use App\Core\Domain\Repository\CannotPersistException;
use App\Core\Domain\Validator\FormValidationException;
use App\Core\Notification\Decorator\TemplateDecorator;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Repository\TemplateRepository;
use App\Core\Notification\UseCase\AddTemplate;
use App\Core\Notification\UseCase\GetTemplate;
use App\Core\Notification\UseCase\UpdateTemplate;
use App\Core\User\Repository\UserRepository;
USE Illuminate\Http\Request;

class TemplateController extends Controller
{
    private $templateRepository;
    private $userRepository;
    private $eventRepository;

    public function __construct(
        TemplateRepository $templateRepository,
        UserRepository $userRepository,
        NotificationEventRepository $eventRepository)
    {
        $this->templateRepository = $templateRepository;
        $this->userRepository = $userRepository;
        $this->eventRepository = $eventRepository;
    }

    public function addTemplateAction(Request $request)
    {
        try {
            $templateRequest = new UseCaseRequest($request->json('template'));
            $createdBy = $this->userRepository->findBy(['api_key' => $request->header('api-key')])[0];
            $response  = (new AddTemplate($this->templateRepository))->addTemplate(
                $templateRequest, $this->userRepository, $this->eventRepository, $createdBy, new TemplateDecorator()
            );
            return response($response->getData(), $response->getCode());
        }catch(FormValidationException $formE){
            return response($formE->getErrors(), 500);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }

    public function getTemplates()
    {
        try{
            $response  =  (new GetTemplate($this->templateRepository))->getAllActiveRecords(new TemplateDecorator());
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function getTemplate($id)
    {
        try{
            $response = (new GetTemplate($this->templateRepository))->getRecord($id, new TemplateDecorator());
            return response($response->getData(), $response->getCode());
        }catch(\Exception $e){
            return response($e->getMessage(), 500);
        }
    }

    public function updateTemplate(Request $request, $id)
    {
        try {
            $templateRequest = new UseCaseRequest($request->json('template'));
            $updatedBy = $this->userRepository->findBy(['api_key' => $request->header('api-key')])[0];
            $response  = (new UpdateTemplate($this->templateRepository))->updateTemplate(
                $id,$templateRequest, $this->userRepository, $this->eventRepository, $updatedBy
            );
            if ($response) {
                return response('Successfully updated!', 200);
            }
            return response('unable to update!', 501);
        }catch(FormValidationException $formE){
            return response($formE->getErrors(), 500);
        }catch(CannotPersistException $persistException){
            return response($persistException->getMessage(), 500);
        }
    }
}
