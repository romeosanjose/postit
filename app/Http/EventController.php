<?php


namespace App\Http\Controllers;


use App\Core\Domain\Boundery\Request as UseCaseRequest;
use App\Core\Domain\Repository\CannotPersistException;
use App\Core\Event\AddEvent;
use App\Core\Event\Event;
use App\Core\Event\Repository\EventRepository;
use App\Core\User\Repository\UserRepository;
use App\Core\Event\Decorator\EventDecorator;
use Illuminate\Http\Request;

class EventController extends Controller {

    private $eventRepository;
    private $userRepository;

    public function __construct(EventRepository $eventRepository, UserRepository $userRepository){
        $this->eventRepository = $eventRepository;
        $this->userRepository = $userRepository;
    }

    public function storeAction(Request $request)
    {
        $eventRequest = new UseCaseRequest($request->json('event'));
        try {
            $createdBy = $this->userRepo->findBy(['api_key' => $request->header('api-key')])[0];
            $response = (new AddEvent($this->eventRepository))->addNewRecord(
                $eventRequest, new Event(), $createdBy, new EventDecorator());
            return response($response->getData(), $response->getCode());
        }catch(FormValidationException $e){
            return response($e->getErrors(), 500);
        }
    }

}
