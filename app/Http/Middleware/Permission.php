<?php

namespace App\Http\Middleware;

use App\Core\User\Repository\UserRepository;
use App\Core\User\User;
use Closure;

class Permission
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Permission constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $users = $this->userRepository->findBy(['api_key' => $request->header('api-key')]);
        if (count($users) > 0) {
            $permissions = config('postit.permissions')[$users[0]->getType()];
            $arrModules =  explode('/', $request->getPathInfo());
            $module = $arrModules[1];

            $return = response('You have no access on this part', 401);

            if (array_key_exists($module, $permissions) && in_array($request->getMethod(), $permissions[$module])) {
                return $next($request);
            }

            return ($this->processOwn($module, $arrModules, $users[0])) ? $next($request) : $return;

        }

        return response('Unauthorized Access', 401);
    }

    private function processOwn($module, $arrModules, User $user)
    {
        if (in_array($module, config('postit.own_permission')) && count($arrModules) > 2){
            if ($user->getId() === $arrModules[2] || $user->getUuid()->toString() === $arrModules[2]) {
                return true;
            }
        }

        return false;
    }
}
