<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 23/08/2019
 * Time: 1:43 PM
 */

namespace App\Core\Post\Decorator;

use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Boundery\Decorator\Decorator;

class PatientDecorator extends Decorator
{
    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }
}
