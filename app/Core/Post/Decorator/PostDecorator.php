<?php


namespace App\Core\Post\Decorator;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Boundery\Decorator\Decorator;

class PostDecorator extends Decorator
{

    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }
}
