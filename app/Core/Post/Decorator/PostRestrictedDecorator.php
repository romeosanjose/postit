<?php


namespace App\Core\Post\Decorator;

use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Boundery\Decorator\Decorator;

class PostRestrictedDecorator extends Decorator
{
    protected function properties()
    {
        return [
            'id',
            'case_job',
            'detailed_case_job',
            'assigned_to',
        ];
    }

    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($this->properties());
    }
}
