<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 22/08/2019
 * Time: 5:46 PM
 */

namespace App\Core\Post;

use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
/**
 * Class Patient
 * @package App\Core\Post
 * @ORM\Entity
 * @ORM\Table(name="patients")
 */
class Patient extends AbstractEntity implements Auditable
{
    use AuditableTrait;

    public function entityProperties()
    {
        return [
            'id',
            'uuid',
            'first_name',
            'last_name',
            'age',
            'gender',
            'background',
            'clinical_department_origin',
            'clinical_department_destination',
            'chart_number',
            'special_indications',
        ];
    }

    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    protected $id;

    /**
     * @var UuidInterface
     * @ORM\Column(type="uuid", unique=true)
     */
    protected $uuid;

    /**
     * @var boolean
     * @ORM\Column(type="string", options={"default" : ""})
     */
    protected $status = "CREATED";

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options={"default": false})
     */
    protected $deleted = false;

    /**
     * @var $first_name;
     * @ORM\Column(type="string")
     */
    private $first_name;

    /**
     * @var $last_name;
     * @ORM\Column(type="string")
     */
    private $last_name;

    /**
     * @var $age
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @var $gender;
     * @ORM\Column(type="string")
     */
    private $gender;

    /**
     * @ORM\Column(type="text")
     */
    private $background;

    /**
     * @var $clinical_department_origin_name;
     * @ORM\Column(type="string", nullable=true)
     */
    private $clinical_department_origin;

    /**
     * @var $clinical_department_destination;
     * @ORM\Column(type="string", nullable=true)
     */
    private $clinical_department_destination;

    /**
     * @var $chart_number;
     * @ORM\Column(type="string", nullable=true)
     */
    private $chart_number;

    /**
     * @var $special_indications
     * @ORM\Column(type="text", nullable=true)
     */
    private $special_indications;

    /**
     * @var $posts
     * @ORM\OneToMany(targetEntity="App\Core\Post\Post", mappedBy="patient")
     *
     */
    private $posts;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return UuidInterface
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param UuidInterface|string $uuid
     *
     * @return $this
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name): void
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name): void
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param mixed $background
     */
    public function setBackground($background): void
    {
        $this->background = $background;
    }

    /**
     * @return mixed
     */
    public function getClinicalDepartmentOrigin()
    {
        return $this->clinical_department_origin;
    }

    /**
     * @param mixed $clinical_department_origin
     */
    public function setClinicalDepartmentOrigin($clinical_department_origin): void
    {
        $this->clinical_department_origin = $clinical_department_origin;
    }

    /**
     * @return mixed
     */
    public function getClinicalDepartmentDestination()
    {
        return $this->clinical_department_destination;
    }

    /**
     * @param mixed $clinical_department_destination
     */
    public function setClinicalDepartmentDestination($clinical_department_destination): void
    {
        $this->clinical_department_destination = $clinical_department_destination;
    }

    /**
     * @return mixed
     */
    public function getChartNumber()
    {
        return $this->chart_number;
    }

    /**
     * @param mixed $chart_number
     */
    public function setChartNumber($chart_number): void
    {
        $this->chart_number = $chart_number;
    }

    /**
     * @return mixed
     */
    public function getSpecialIndications()
    {
        return $this->special_indications;
    }

    /**
     * @param mixed $special_indications
     */
    public function setSpecialIndications($special_indications): void
    {
        $this->special_indications = $special_indications;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
        $post->setPatient($this);
    }

    public function removePost(Post $post)
    {
        // ...
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }

    public function __construct()
    {

        $this->posts = new ArrayCollection();

    }

}
