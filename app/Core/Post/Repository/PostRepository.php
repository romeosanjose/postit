<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 22/08/2019
 * Time: 5:57 PM
 */

namespace App\Core\Post\Repository;

use App\Core\Domain\Repository\Repository;
use App\Core\User\User;

interface PostRepository extends Repository
{

    public function findActivePosts();

    public function updatePostsStatus($id, User $updatedBy);

}
