<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 08/08/2019
 * Time: 2:42 PM
 */

namespace App\Core\Post\Repository;

use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\Post\Post;
use App\Core\User\User;

class PostDoctrineRepository extends DoctrineRepository implements PostRepository
{
    public function entity()
    {
        return Post::class;
    }

    public function findActivePosts()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('post')
            ->from(Post::class, 'post')
            ->where('post.deleted = 0')
            ->andWhere("post.status = 'CREATED' OR post.status = 'ACCEPTED' OR post.status = 'DONE'");
        return $qb->getQuery()->execute();
    }

    public function updatePostsStatus($id, User $updatedBy)
    {
        $qb = $this->entityManager->createQueryBuilder();
        $q = $qb->update(Post::class, 'post')
                ->set('post.status', ':status')
                ->set('post.assigned_to', ':updatedBy')
                ->where('post.uuid = :uuid')
                ->setParameter('uuid', $id)
                ->setParameter('status', 'ACCEPTED')
                ->setParameter('updatedBy', $updatedBy->getId())
                ->getQuery();
         return $q->execute();
    }


}
