<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 08/08/2019
 * Time: 2:42 PM
 */

namespace App\Core\Post\Repository;


use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\Post\Patient;



class PatientDoctrineRepository extends DoctrineRepository implements PatientRepository
{
    public function entity()
    {
        return Patient::class;
    }

    public function findActivePatients()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('patient')
            ->from(Patient::class, 'patient')
            ->where('patient.deleted = 0');
        return $qb->getQuery()->execute();
    }

}
