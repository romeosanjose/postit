<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 22/08/2019
 * Time: 5:40 PM
 */

namespace App\Core\Post;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 * @package App\Core\Post
 * @ORM\Entity
 * @ORM\Table(name="posts")
 */
class Post extends AbstractEntity implements Identity, Auditable
{
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Post\Patient", inversedBy="posts")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     */
    private $patient;

    /**
     * @ORM\Column(type="text")
     */
    private $case_job;

    /**
     * @ORM\Column(type="text")
     */
    private $detailed_case_job;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\User\User")
     * @ORM\JoinColumn(name="assigned_to", referencedColumnName="id")
     */
    private $assigned_to;

    /**
     * @return mixed
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * @param mixed $patient
     */
    public function setPatient($patient): void
    {
        $this->patient = $patient;
    }

    /**
     * @return mixed
     */
    public function getCaseJob()
    {
        return $this->case_job;
    }

    /**
     * @param mixed $case
     */
    public function setCaseJob($case_job): void
    {
        $this->case_job = $case_job;
    }

    /**
     * @return mixed
     */
    public function getDetailedCaseJob()
    {
        return $this->detailed_case_job;
    }

    /**
     * @param mixed $detailed_case_job
     */
    public function setDetailedCaseJob($detailed_case_job): void
    {
        $this->detailed_case_job = $detailed_case_job;
    }

    /**
     * @return mixed
     */
    public function getAssignedTo()
    {
        return $this->assigned_to;
    }

    /**
     * @param mixed $assignedTo
     */
    public function setAssignedTo($assignedTo): void
    {
        $this->assigned_to = $assignedTo;
    }



    public function entityProperties()
    {
        return [
            'id',
            'case_job',
            'detailed_case_job',
            'assigned_to',
            'patient'
        ];

    }
}
