<?php


namespace App\Core\Post\UseCase;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Post\Decorator\PatientDecorator;

class GetPatient extends UseCase
{
    public function getPatient($id, Decorator $decorator = null)
    {
        $patient = $this->repository->findBy(['id' => $id, 'deleted' => false]);
        if ($patient){
            $patient = $patient[0];
        }
        if ($decorator) {
            $patientData = $decorator->decorate($patient);
            return $this->response($patientData);
        }

        return $patient;
    }

    public function getPatients()
    {
        $patientObjects = $this->repository->findActivePatients();
        $patientLists = [];
        foreach ($patientObjects as $patientObject) {
            $patientLists[] = (new PatientDecorator())->decorate($patientObject);
        }
        $response = new Response($patientLists);
        $response->setCode(200);

        return $response;
    }
}
