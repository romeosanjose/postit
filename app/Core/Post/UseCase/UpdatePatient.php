<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/09/2019
 * Time: 5:30 PM
 */

namespace App\Core\Post\UseCase;


use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Post\Patient;
use App\Core\User\User;

class UpdatePatient extends UseCase
{
    public function updatePatient(
        Request $patientRequest,
        User $currentUser
    )
    {
        $patientData = $patientRequest->getData();
        $patient = (new GetPatient($this->repository))->getPatient($patientData['id']);
        $patient = $patient->fromArray($patientData);
        $patient = $this->setEditor($patient, $currentUser);

        return $this->repository->update($patient, Patient::class);
    }

}
