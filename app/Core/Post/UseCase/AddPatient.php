<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 22/08/2019
 * Time: 6:06 PM
 */

namespace App\Core\Post\UseCase;

use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\UseCase\UseCase;
use App\Core\User\User;

class AddPatient extends UseCase
{

    protected $validationRules = [
        'id' => [
            'required',
            'max_length(50)'
        ],
    ];


}
