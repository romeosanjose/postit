<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 04/09/2019
 * Time: 6:35 AM
 */

namespace App\Core\Post\UseCase;

use App\Core\Domain\UseCase\UseCase;
use App\Core\User\User;

class AcceptPost extends UseCase
{
    public function acceptPost($id, User $acceptedBy)
    {
        $result = $this->repository->updatePostsStatus($id, $acceptedBy);
        $code = 200;
        if (!$result) {
            $code = 501;
        }
        return $this->response($result, $code);
    }
}
