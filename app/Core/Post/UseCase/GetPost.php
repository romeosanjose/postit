<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 27/08/2019
 * Time: 9:03 AM
 */

namespace App\Core\Post\UseCase;

use App\Core\Domain\Boundery\Request;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Post\Decorator\PostDecorator;
use App\Core\Post\Decorator\PostRestrictedDecorator;


class GetPost extends UseCase
{
    public function getPosts()
    {
        $postObjects = $this->repository->findActivePosts();
        $postLists = [];
        foreach ($postObjects as $postObject) {

            $postLists[] = ($postObject->getStatus() == 'ACCEPTED')
                ? (new PostDecorator())->decorate($postObject)
                : (new PostRestrictedDecorator())->decorate($postObject);
        }

        return $this->response($postLists);
    }

    public function getPost($id)
    {
        $postObject = $this->repository->findByUuid($id);
//        $post = ($postObject->getStatus() == 'ACCEPTED')
//            ? (new PostDecorator())->decorate($postObject)
//            : (new PostRestrictedDecorator())->decorate($postObject);

        $post = (new PostDecorator())->decorate($postObject);
        return $this->response($post);
    }

    public function getPostByTreatment($treatment){
        $postObjects = $this->repository->findBy(['case_job' => $treatment]);
        $postLists = [];
        foreach ($postObjects as $postObject) {

            $postLists[] = ($postObject->getStatus() == 'ACCEPTED')
                ? (new PostDecorator())->decorate($postObject)
                : (new PostRestrictedDecorator())->decorate($postObject);
        }

        return $this->response($postLists);
    }
}
