<?php


namespace App\Core\Post\UseCase;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\Repository\FileRepository;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Post\Decorator\PostDecorator;
use App\Core\Post\Patient;
use App\Core\Post\Post;
use App\Core\Post\Repository\PatientRepository;
use App\Core\User\Repository\PersonRepository;
use App\Core\User\User;

class AddPost extends UseCase
{
    protected $validationRules = [
        'case_job' => [
            'required'
        ]
    ];

    public function addPost(
        Request $request,
        PatientRepository $patientRepository,
        User $currentUser
    )
    {
        $postData = $request->getData();
        $this->validateUserData($postData);
        $patient = $this->getPatientObject($postData['patient'], $patientRepository, $currentUser);
        $postData['patient'] = $patient;

        return $this->addNewRecord(new Request($postData), new Post, $currentUser, new PostDecorator());
    }

    protected function getPatientObject($patientData, PatientRepository $patientRepository, User $currentUser)
    {
        $patient = (new GetPatient($patientRepository))->getPatient($patientData['id']);
        if (!$patient){
            $patient  = (new AddPatient($patientRepository))->addNewRecord(
                new Request($patientData),
                new Patient(),
                $currentUser
            );

            return $patient;
        }

        if ((new UpdatePatient($patientRepository))->updatePatient(
            new Request($patientData),
            $currentUser
        )) {
            return (new GetPatient($patientRepository))->getPatient($patientData['id']);
        }

        return $patient;
    }
}
