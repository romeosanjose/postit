<?php


namespace App\Core\Post\UseCase;


use App\Core\Domain\Boundery\Request;
use App\Core\Post\Post;
use App\Core\Post\Repository\PatientRepository;
use App\Core\User\User;

class UpdatePost extends AddPost
{
    public function updatePost($id, Request $request, PatientRepository $patientRepository, User $user = null)
    {
        $postData = $request->getData();
        $patient = $this->getPatientObject($postData['patient'], $patientRepository, $user);
        $post = (new GetPost($this->repository))->getRecord($id);
        unset($postData['patient']);
        $post = $post->fromArray($postData);
        $post->setPatient($patient);

        return $this->repository->update($post, Post::class);
    }
}
