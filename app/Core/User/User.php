<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/03/2019
 * Time: 12:17 PM
 */


namespace App\Core\User;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package App\Core\User
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends AbstractEntity implements Identity, Auditable
{
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $salt;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $log_attempt = 0;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $locked = false;
    /**
    * @ORM\OneToOne(targetEntity="App\Core\User\Person")
    * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
    **/
    private $person;

    /**
     * @ORM\Column(type="string")
     */
    private $api_key;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    private $passwordSend;

    /**
     * @return array
     */
    public function entityProperties()
    {
        return [
            'email',
            'log_attempt',
            'locked',
            'person',
            'type'
        ];
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogAttempt()
    {
        return $this->log_attempt;
    }

    /**
     * @param mixed $logAttempt
     */
    public function setLogAttempt($logAttempt)
    {
        $this->log_attempt = $logAttempt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param mixed $locked
     */
    public function setLocked(bool $locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return mixed
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param mixed $person
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    public function encryptPassword($password, $salt)
    {
        return sha1($password . $salt);
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @param mixed $api_key
     */
    public function setApiKey($api_key): void
    {
        $this->api_key = $api_key;
    }

    /**
     * @return mixed
     */
    public function getPasswordSend()
    {
        return $this->passwordSend;
    }

    /**
     * @param mixed $passwordSend
     */
    public function setPasswordSend($passwordSend): void
    {
        $this->passwordSend = $passwordSend;
    }



    public function authenticate($email, $password)
    {
        $isValid = false;
        if ($email === $this->getEmail()){
            if ($this->encryptPassword($password, $this->getSalt()) === $this->getPassword()){
                $isValid = true;
            }
        }

        return $isValid;
    }
}
