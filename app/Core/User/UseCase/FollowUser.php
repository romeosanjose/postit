<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 25/09/2019
 * Time: 6:04 AM
 */

namespace App\Core\User\UseCase;

use App\Core\Domain\UseCase\UseCase;
use App\Core\User\User;

class FollowUser extends UseCase
{
    public function follow($uuuid, User $currentUser)
    {
        $userToFollow = $this->getRecord($uuuid);
        $currentUser->follow($userToFollow);
        return $this->repository->update($currentUser, User::class);
    }

    public function unfollow($uuid, User $currenUser)
    {
        $followedUser = $this->getRecord($uuid);
        $currenUser->unfollow($followedUser);
        return $this->repository->update($currenUser, User::class);
    }
}
