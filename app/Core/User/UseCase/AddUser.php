<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/06/2019
 * Time: 8:27 AM
 */

namespace App\Core\User\UseCase;

use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Repository\TemplateRepository;
use App\Core\User\Decorator\UserProtectedDecorator;
use App\Core\User\Events\UserCreated;
use App\Core\User\User;
use App\Events\Event;
use App\Core\User\UseCase\GeneratePassword;
use Hackzilla\PasswordGenerator\Generator\HumanPasswordGenerator;


class AddUser extends UseCase
{
    protected $validationRules = [
        'email' => [
            'required',
            'email'
        ],
        'type' => [
            'required'
        ]
    ];

    /**
     * @param Request $request
     * @param User $createdBy
     * @return Response|mixed
     */
    public function addUser(Request $request, User $createdBy, NotificationEventRepository $eventRepository, TemplateRepository $templateRepository)
    {
        $data = $request->getData();
        $this->validateUserData($data);

        if ($this->isExists($data['email'], 'email')) {
            return $this->response('email already exists!', 409);
        }

        $user = (new User())->fromArray($data);
        $generatePasswordCommand = new GeneratePassword(new HumanPasswordGenerator());
        $password = $generatePasswordCommand->generatePassword();
        $user->setPassword($password);
        $user->setPasswordSend($password);
        $user = $this->encryptUserPassword($user, $user->getPassword());
        $user->setApiKey(bin2hex(openssl_random_pseudo_bytes(64)));
        $response = $this->addNewRecord(new Request($data), $user, $createdBy, new UserProtectedDecorator(), $validated = true);
        event(new UserCreated($user, $eventRepository, $templateRepository ));

        return $response;
    }

    private function encryptUserPassword(User $user, $password)
    {
        $user->setSalt(sha1($password));
        $user->setPassword(sha1($password . $user->getSalt()));

        return $user;
    }

    private function isExists($needle, $key)
    {
        $userObjs = $this->repository->findBy([$key => $needle]);
        if (count($userObjs) > 0) {
            return true;
        }

        return false;
    }


}
