<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 05/09/2019
 * Time: 10:14 AM
 */

namespace App\Core\User\UseCase;


use App\Core\User\Decorator\UserProtectedDecorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;
use App\Core\User\User;

class LoginUser extends UseCase
{
    protected $validationRules = [
        'email' => [
            'required'
        ],
        'password' => [
            'required'
        ],
    ];

    public function doLogin(Request $request)
    {
        $data = $request->getData();
        $this->validateUserData($data);
        $userObjs = $this->repository->findBy(['email' => $data['email']]);

        if ($userObjs && $userObjs[0]->isLocked()){
            $response = new Response(config('dictionary')['user_locked']);
            $response->setCode(401);
            return $response;
        }

        if ($userObjs && $userObjs[0]->authenticate($data['email'], $data['password'])) {
            $response = new Response((new UserProtectedDecorator())->decorate($userObjs[0]));
            $response->setCode(200);
            return $response;
        }

        if ($userObjs && $this->logIncorrectAttempt($userObjs[0]) === 3) {
            $response = new Response(config('dictionary')['reached_maximum_attempt']);
        } else {
            $response = new Response(config('dictionary')['incorrect_access']);
        }

        $response->setCode(401);
        return $response;
    }


    private function logIncorrectAttempt(User $user)
    {
        $attempt = $user->getLogAttempt() + 1;
        $updateFields = ['log_attempt' => $attempt];
        if ($attempt === 3) {
            $updateFields = ['log_attempt' => $attempt, 'locked' => true];
        }

        $this->repository->updateBy($updateFields, ['id' => $user->getId()]);
        return $attempt;
    }
}
