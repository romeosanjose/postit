<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 18/09/2019
 * Time: 9:47 PM
 */

namespace App\Core\User\UseCase;


use App\Core\Domain\Boundery\Request;
use App\Core\File\File;
use App\Core\User\Person;
use App\Core\User\User;


class UpdatePerson extends AddPerson
{
    public function updatePerson( Request $request, $file, User $updateBy)
    {

        $personData = $request->getData();
        $person = $this->getRecord($personData['id']);
        unset($personData['file']);
        $person = $person->fromArray($personData);
        if (!is_null($file)){
            $person->setProfilePicture($file);
        }
        $person  = $this->setEditor($person, $updateBy);

        return $this->repository->update($person, Person::class);
    }
}
