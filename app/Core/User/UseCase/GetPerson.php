<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 18/09/2019
 * Time: 10:33 PM
 */

namespace App\Core\User\UseCase;


use App\Core\Domain\UseCase\UseCase;

class GetPerson extends UseCase
{
    public function getPerson($id)
    {
        return $this->repository->find($id);
    }
}