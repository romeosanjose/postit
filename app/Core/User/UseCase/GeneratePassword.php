<?php


namespace App\Core\User\UseCase;


use Hackzilla\PasswordGenerator\Generator\AbstractPasswordGenerator;


class GeneratePassword
{
    private $passwordGenerator;

    public function __construct(AbstractPasswordGenerator $passwordGenerator)
    {
        $this->passwordGenerator = $passwordGenerator;
    }

    public function generatePassword()
    {
        $this->passwordGenerator
            ->setWordList('/usr/share/dict/words')
            ->setWordCount(2)
            ->setWordSeparator('-');

        return $this->passwordGenerator->generatePasswords()[0];
    }
}
