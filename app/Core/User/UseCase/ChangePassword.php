<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 23/09/2019
 * Time: 1:18 PM
 */

namespace App\Core\User\UseCase;

use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;

class ChangePassword extends UseCase
{

    protected $validationRules = [
        'password' => [
            'required',
        ],
        'password_repeat' => [
            'required'
        ]
    ];

    public function changePassword(Request $request, $uuid)
    {
        if (!$this->validatePassword($request->get('password'), $request->get('password_repeat'))) {
            $response = new Response('Password does not matched');
            $response->setCode(501);
            return $response;
        }

        $password = $request->get('password');
        $salt = sha1($password);
        $newpassword = sha1($password . $salt);

        if ($this->repository->updateBy(['password' => $newpassword, 'salt' => $salt],
            ['uuid' => $uuid])) {
            $response = new Response('Successfully change password!');
            $response->setCode(200);
        } else {
            $response = new Response('unable to change password');
            $response->setCode(501);
        }

        return $response;
    }

    private function validatePassword($password, $password_repeat){
        return ($password === $password_repeat);
    }
}
