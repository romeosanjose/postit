<?php


namespace App\Core\User\UseCase;

use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Repository\TemplateRepository;
use App\Core\User\Events\PasswordChangeExtracted;

class SendPasswordLink extends UseCase
{
    public function getLinkFromEmail($email, NotificationEventRepository $eventRepository, TemplateRepository $templateRepository)
    {
        $user = $this->repository->findBy(['email' => $email]);
        event(new PasswordChangeExtracted($user[0], $eventRepository, $templateRepository ));
    }
}
