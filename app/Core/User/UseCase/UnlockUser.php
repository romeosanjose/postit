<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 23/09/2019
 * Time: 1:17 PM
 */

namespace App\Core\User\UseCase;


use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;

class UnlockUser extends UseCase
{
    public function unlockAccount($id)
    {
        if ($this->repository->updateBy(['log_attempt' => 0, 'locked' => false], ['id' => $id])) {
            $response = new Response('Successfully unlocked account!');
            $response->setCode(200);
        } else {
            $response = new Response('unable to unlock account');
            $response->setCode(501);
        }

        return $response;
    }
}