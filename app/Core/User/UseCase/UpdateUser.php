<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 18/09/2019
 * Time: 9:48 PM
 */

namespace App\Core\User\UseCase;

use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\User\Person;
use App\Core\User\User;

class UpdateUser extends AddUser
{

    protected $validationRules = [
        'username' => [
            'required',
            'max_length(50)'
        ],
        'email' => [
            'required',
            'email'
        ],
        'type' => [
            'required'
        ]
    ];

    public function updateUser(Request $request, Person $person, User $updatedBy)
    {
        $userData = $request->getData();
        $user = $this->getRecord($userData['id']);
        unset($userData['person']);
        $user = $user->fromArray($userData);
        $user->setPerson($person);
        $user  = $this->setEditor($user, $updatedBy);

        return $this->repository->update($user, User::class);
    }
}
