<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 27/08/2019
 * Time: 9:03 AM
 */

namespace App\Core\User\UseCase;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\UseCase\UseCase;
use App\Core\User\Decorator\UserProtectedDecorator;
use App\Core\Domain\Boundery\Response;

class GetUser extends UseCase
{
    public function getUsers()
    {
        $userObjects = $this->repository->findActiveRecords();

        $userLists = [];
        foreach ($userObjects as $userObject) {
            $userLists[] = (new UserProtectedDecorator())->decorate($userObject);
        }

        return $this->response($userLists);
    }

    public function getUserByEmail($email)
    {
        $userObjects = $this->repository->findBy(['email' => $email]);
        $userLists = [];
        foreach ($userObjects as $userObject) {
            $userLists[] = (new UserProtectedDecorator())->decorate($userObject);
        }

        return $this->response($userLists);
    }

}
