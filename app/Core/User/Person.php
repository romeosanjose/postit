<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/03/2019
 * Time: 12:03 PM
 */

namespace App\Core\User;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Person
 * @package App\Core\User
 * @ORM\Entity
 * @ORM\Table(name="persons")
 */
class Person extends AbstractEntity implements Identity, Auditable
{
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $first_name;
    /**
     * @ORM\Column(type="string")
     */
    private $last_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Setting\ManageList")
     * @ORM\JoinColumn(name="shift_color", referencedColumnName="id")
     *
     */
    private $shift_color;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Setting\ManageList")
     * @ORM\JoinColumn(name="clinical_department", referencedColumnName="id")
     *
     */
    private $clinical_department;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Setting\ManageList")
     * @ORM\JoinColumn(name="year_level", referencedColumnName="id")
     *
     */
    private $year_level;

    /**
     * One Person has One Profile.
     * @ORM\OneToOne(targetEntity="App\Core\File\File", mappedBy="Person")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="id")
     */
    private $profile_picture;

    public function entityProperties()
    {
        return [
            'id',
            'first_name',
            'last_name',
            'shift_color',
            'clinical_department',
            'year_level',
            'profile_picture',
        ];
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
        return $this;
    }


    public function getShiftColor()
    {
        return $this->shift_color;
    }

    public function setShiftColor($shiftColor)
    {
        $this->shift_color = $shiftColor;
    }

    public function getClinicalDepartment()
    {
        return $this->clinical_department;
    }

    public function setClinicalDepartment($clinical_department)
    {
        $this->clinical_department = $clinical_department;
    }

    public function getYearLevel()
    {
        return $this->year_level;
    }

    public function setYearLevel($year_level)
    {
        $this->year_level = $year_level;
    }

    /**
     * @return mixed
     */
    public function getProfilePicture()
    {
        return $this->profile_picture;
    }

    /**
     * @param mixed $profile_picture
     */
    public function setProfilePicture($profile_picture): void
    {
        $this->profile_picture = $profile_picture;
    }




}
