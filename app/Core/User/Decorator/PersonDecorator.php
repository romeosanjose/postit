<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/06/2019
 * Time: 3:05 PM
 */

namespace App\Core\User\Decorator;

use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Entity\AbstractEntity;


class PersonDecorator extends Decorator
{

    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }

}
