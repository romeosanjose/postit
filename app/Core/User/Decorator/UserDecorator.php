<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 28/06/2019
 * Time: 11:33 AM
 */

namespace App\Core\User\Decorator;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Entity\AbstractEntity;

class UserDecorator extends Decorator
{
    protected function properties()
    {
        return [
            'id',
            'password',
            'api_key',
            'salt',
            'email',
            'log_attempt',
            'locked',
            'person',
            'deleted',
            'type',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at'
        ];
    }


    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($this->properties());
    }
}
