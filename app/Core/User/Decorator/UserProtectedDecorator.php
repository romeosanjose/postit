<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 20/08/2019
 * Time: 5:15 PM
 */

namespace App\Core\User\Decorator;


class UserProtectedDecorator extends UserDecorator
{
    protected function properties()
    {
        return [
            'id',
            'email',
            'api_key',
            'log_attempt',
            'locked',
            'person',
            'status',
            'deleted',
            'type',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at'
        ];
    }
}
