<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 08/08/2019
 * Time: 2:42 PM
 */

namespace App\Core\User\Repository;


use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\User\User;
use App\Core\User\Repository\UserRepository;


class UserDoctrineRepository extends DoctrineRepository implements UserRepository
{
    public function entity()
    {
        return User::class;
    }
}
