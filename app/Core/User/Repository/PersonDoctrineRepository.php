<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 08/08/2019
 * Time: 2:42 PM
 */

namespace App\Core\User\Repository;


use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\User\Person;
use App\Core\User\Repository\PersonRepository;


class PersonDoctrineRepository extends DoctrineRepository implements PersonRepository
{
    public function entity()
    {
        return Person::class;
    }

}
