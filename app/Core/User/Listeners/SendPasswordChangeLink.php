<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 24/09/2019
 * Time: 2:52 PM
 */

namespace App\Core\User\Listeners;


use App\Core\User\Events\PasswordChangeExtracted;
use App\Core\User\Events\UserCreated;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\Listener;
use Illuminate\Support\Facades\Mail;

class SendPasswordChangeLink
{
    public function __construct()
    {
    }

    public function handle(PasswordChangeExtracted $event)
    {
        $entity = $event->getRootEntity();
//        if ($events = $event->getEventRepository()->findBy(['name' => $event->getEventName()])){
//            if ($templates = $event->getTemplateRepository()->findBy(['event_id' => $events[0]->getId()])) {
//                foreach ($templates as $template) {
                    Mail::send([],[],function($message) use ($entity){
                        $message->to($entity->getEmail())
                            ->subject('Password Change')
                            ->setBody("
                                    Go to this link to change password:
                                    http://localhost:3000/password/change/{$entity->getUuid()}'
                            ");
                    });
//                }
//            }
//        }

    }


}
