<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 24/09/2019
 * Time: 2:52 PM
 */

namespace App\Core\User\Listeners;


use App\Core\User\Events\UserCreated;
use Illuminate\Support\Facades\Mail;

class SendUserCreatedEmail
{
    public function __construct()
    {
    }

    public function handle(UserCreated $event)
    {
        $entity = $event->getRootEntity();
//        if ($events = $event->getEventRepository()->findBy(['name' => $event->getEventName()])){
//            if ($templates = $event->getTemplateRepository()->findBy(['event_id' => $events[0]->getId()])) {
//                foreach ($templates as $template) {
                    Mail::send([],[],function($message) use ($entity){
                        $message->to($entity->getEmail())
                            ->subject('Account Created')
                            ->setBody("
                                <p> hello, </p><br/>
                                <p> Your account was successfull created! </p><br/>
                                password : {$entity->getPasswordSend()}

                                <br/><br/>
                                regards, <br/>
                                Cura Administrator
                                "
                                , 'text/html');
                    });
//                }
//            }
//        }

    }


}
