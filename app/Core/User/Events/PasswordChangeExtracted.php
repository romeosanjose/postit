<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 24/09/2019
 * Time: 2:07 PM
 */

namespace App\Core\User\Events;

use App\Core\Domain\Event\Notifiable;
use App\Core\Notification\Event\NotificationEvent;
use App\Core\User\User;
use App\Events\Event;
use Illuminate\Support\Facades\Mail;

class PasswordChangeExtracted extends NotificationEvent implements Notifiable
{
    public function getEventName()
    {
        return PasswordChangeExtracted::class;
    }
}
