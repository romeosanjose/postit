<?php

namespace App\Core\Event\UseCase;

use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\UseCase\UseCase;
use App\Core\User\User;

class AddEvent extends UseCase {

    protected $validationRules = [
        
        'name' => [
            'required',
        ],
        'date_time' => [
            'required',
        ],
        'venue' => [
            'required',
        ],
    ];
}