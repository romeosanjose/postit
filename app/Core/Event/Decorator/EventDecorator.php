<?php


namespace App\Core\Event\Decorator;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Boundery\Decorator\Decorator;

class EventDecorator extends Decorator
{

    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }
}