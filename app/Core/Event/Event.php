<?php
namespace App\Core\Event;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Event
 * @package App\Core\Event
 * @ORM\Entity
 * @ORM\Table(name="events")
 */
class Event extends AbstractEntity implements Identity, Auditable
{
    use IdentityTrait;
    use AuditableTrait;

     /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateTime;

     /**
     * @ORM\Column(type="text")
     */
    private $venue;
}