<?php


namespace App\Core\Event\Repository;

use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\Event\Event;


class EventDoctrineRepository extends DoctrineRepository implements EventRepository
{
    public function entity()
    {
        return Event::class;
    }

}