<?php


namespace App\Core\Setting\Repository;

use App\Core\Domain\Repository\Repository;


interface ManageListRepository extends Repository
{
    public function getListByModule($module);
}
