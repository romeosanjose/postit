<?php


namespace App\Core\Setting\Repository;

use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\Setting\ManageList;


class ManageListDoctrineRepository extends DoctrineRepository implements ManageListRepository
{
    public function entity()
    {
        return ManageList::class;
    }

    public function getListByModule($module)
    {

    }

}
