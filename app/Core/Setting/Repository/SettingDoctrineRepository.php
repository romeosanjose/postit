<?php


namespace App\Core\Setting\Repository;

use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\Setting\Setting;


class SettingDoctrineRepository extends DoctrineRepository implements SettingRepository
{
    public function entity()
    {
        return Setting::class;
    }

}