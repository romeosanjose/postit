<?php


namespace App\Core\Setting\Decorator;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Boundery\Decorator\Decorator;

class ManageListDecorator extends Decorator
{

    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }
}