<?php

namespace App\Core\Setting;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class ManageList
 * @package App\Core\Setting
 * @ORM\Entity
 * @ORM\Table(name="manage_lists")
 */

class ManageList extends AbstractEntity implements Identity, Auditable {
    use IdentityTrait;
    use AuditableTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $module;


     public function entityProperties()
    {
        return [
            'name',
            'description',
            'module',
        ];
    }

    public function getName()
    {
    	return $this->name;
    }

    public function setName($name)
    {
    	$this->name = $name;
    }

    public function getDescription()
    {
    	return $this->description;
    }

    public function setDescription($description)
    {
    	$this->description = $description;
    }

    public function getModule()
    {
    	return $this->module;
    }

    public function setModule($module)
    {
    	$this->module = $module;
    }
}
