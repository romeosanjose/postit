<?php

namespace App\Core\Setting\UseCase;

use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\UseCase\UseCase;
use App\Core\User\User;

class AddSetting extends UseCase {

    protected $validationRules = [
        
        'name' => [
            'required',
        ],
        'data' => [
            'required',
        ],
    ];
}