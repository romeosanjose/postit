<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 27/08/2019
 * Time: 9:03 AM
 */

namespace App\Core\Setting\UseCase;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Setting\Decorator\ManageListDecorator;
use App\Core\Domain\Boundery\Response;

class GetManageList extends UseCase
{
    public function getManageLists($module)
    {
        $manageListObjs = $this->repository->findBy(['module' => $module]);
        $manageLists = [];
        foreach ($manageListObjs as $manageListObj) {
            $manageLists[] = (new ManageListDecorator())->decorate($manageListObj);
        }

        $response = new Response($manageLists);
        $response->setCode(200);

        return $response;
    }
}
