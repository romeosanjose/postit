<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 24/09/2019
 * Time: 4:37 PM
 */

namespace App\Core\Notification;

use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use App\Core\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Template
 * @package App\Core\Notification
 * @ORM\Entity
 * @ORM\Table(name="notification_templates")
 */
class Template extends AbstractEntity implements Identity, Auditable
{
    use AuditableTrait;
    use IdentityTrait;

    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $subject;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\ManyToMany(targetEntity="App\Core\User\User", inversedBy="user")
     * @ORM\JoinTable(name="link_notification_recipients",
     *      joinColumns={@ORM\JoinColumn(name="template_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     **/
    private $recipients;

    /**
     * @ORM\ManyToOne(targetEntity="App\Core\Notification\NotificationEvent", inversedBy="templates")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    private $event;


    public function __construct()
    {
        $this->recipients = new ArrayCollection();
    }

    public function entityProperties()
    {
       return [
           'name',
           'subject',
           'message',
           'recipients',
           'event',
       ];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param mixed $recipients
     */
    public function addRecipients(User $recipients): void
    {
        $this->recipients[] = $recipients;
    }

    public function replaceRecipients(ArrayCollection $recipients) : void
    {
        $this->recipients = $recipients;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event): void
    {
        $this->event = $event;
    }



}
