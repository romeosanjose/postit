<?php


namespace App\Core\Notification\UseCase;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\UseCase\UseCase;
use App\Core\Domain\Validator\FormValidationException;
use App\Core\Notification\Decorator\TemplateDecorator;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Template;
use App\Core\User\Repository\UserRepository;
use App\Core\User\User;
use Doctrine\Common\Collections\ArrayCollection;

class AddTemplate extends UseCase
{
    protected $validationRules = [
        'name' => [
            'required'
        ],
        'subject' => [
            'required'
        ],
        'message' => [
            'required'
        ],
        'event' => [
            'required'
        ]
    ];

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @param NotificationEventRepository $eventRepository
     * @param User $createdBy
     * @param Decorator|null $decorator
     * @return mixed
     */
    public function addTemplate(
        Request $request,
        UserRepository $userRepository,
        NotificationEventRepository $eventRepository,
        User $createdBy,
        Decorator $decorator = null
    )
    {
        $templateData = $request->getData();
        $this->validate($templateData);
        $templateData['recipients'] = $this->setRecipients($templateData, $userRepository);
        $templateData['event'] = $eventRepository->find($templateData['event']);
        $request->setData($templateData);

        return $this->response(
            $this->addNewRecord($request, new Template(), $createdBy, $decorator, $validated = true)
        );
    }

    protected function setRecipients($templateData, UserRepository $userRepository)
    {
        $recipients = new ArrayCollection();
        foreach($templateData['recipients'] as $id) {
            $recipients[] = $userRepository->find($id);
        }
        return $recipients;
    }

    protected function validate($templateData)
    {
        $this->validateUserData($templateData);
        $key = 'recipients';
        if (is_null($templateData[$key]) || empty($templateData[$key]) || count($templateData[$key]) == 0) {
            throw new FormValidationException($templateData[$key] . 'is required');
        }

    }
}
