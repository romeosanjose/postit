<?php


namespace App\Core\Notification\UseCase;


use App\Core\Domain\Boundery\Request;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Template;
use App\Core\User\Repository\UserRepository;
use App\Core\User\User;

class UpdateTemplate extends AddTemplate
{
    public function updateTemplate(
        $id,
        Request $request,
        UserRepository $userRepository,
        NotificationEventRepository $eventRepository,
        User $updateBy
    ){
        $templateData = $request->getData();
        $templateData['event'] = $eventRepository->find($templateData['event']);
        $recipients = $this->setRecipients($templateData, $userRepository);
        unset($templateData['recipients']);
        $template = $this->getRecord($id);
        $template = $template->fromArray($templateData);
        $template->replaceRecipients($recipients);
        $template->setUpdatedBy($updateBy);
        $template->setUpdatedAt(new \DateTime('now'));

        return $this->repository->update($template, Template::class);
    }
}
