<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 26/09/2019
 * Time: 12:25 PM
 */

namespace App\Core\Notification\UseCase;


use App\Core\Domain\UseCase\UseCase;

class AddNotificationEvent extends UseCase
{
    protected $validationRules = [
        'name' => [
            'required'
        ],
        'event' => [
            'required'
        ],
    ];
}