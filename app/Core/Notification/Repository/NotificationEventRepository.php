<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 26/09/2019
 * Time: 12:21 PM
 */

namespace App\Core\Notification\Repository;


use App\Core\Domain\Repository\Repository;

interface NotificationEventRepository extends Repository
{

}