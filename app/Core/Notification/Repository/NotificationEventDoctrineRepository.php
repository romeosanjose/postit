<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 26/09/2019
 * Time: 12:21 PM
 */

namespace App\Core\Notification\Repository;


use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\Notification\NotificationEvent;

class NotificationEventDoctrineRepository extends DoctrineRepository implements NotificationEventRepository
{

    public function entity()
    {
        return NotificationEvent::class;
    }
}