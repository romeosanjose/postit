<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 26/09/2019
 * Time: 12:20 PM
 */

namespace App\Core\Notification\Repository;


use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\Notification\Template;

class TemplateDoctrineRepository extends DoctrineRepository implements TemplateRepository
{
    public function entity()
    {
        return Template::class;
    }



}
