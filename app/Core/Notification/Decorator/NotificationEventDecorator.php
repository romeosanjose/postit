<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 26/09/2019
 * Time: 11:59 AM
 */

namespace App\Core\Notification\Decorator;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Entity\AbstractEntity;

class NotificationEventDecorator extends Decorator
{
    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }
}