<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 24/09/2019
 * Time: 2:57 PM
 */

namespace App\Core\Notification;

use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class NotificationEvent
 * @package App\Core\Notification
 * @ORM\Entity
 * @ORM\Table(name="notification_events")
 */
class NotificationEvent extends AbstractEntity implements Auditable, Identity
{
    use AuditableTrait;
    use IdentityTrait;

    /**
     * @var $name
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $event;

    /**
     * @var $templates
     * @ORM\OneToMany(targetEntity="App\Core\Notification\Template", mappedBy="event")
     *
     */
    private $templates;


    public function __construct()
    {
        $this->templates = new ArrayCollection();
    }

    public function entityProperties()
    {
        return [
            'id',
            'name',
            'event',
            'templates',
        ];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event): void
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * @param mixed $templates
     */
    public function addTemplates($templates): void
    {
        $this->templates[] = $templates;
    }


}
