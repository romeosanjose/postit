<?php


namespace App\Core\Notification\Event;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Notification\Repository\NotificationEventRepository;
use App\Core\Notification\Repository\TemplateRepository;

class NotificationEvent
{
    protected $eventRepository;

    protected $templateRepository;

    protected $entity;

    public function __construct(AbstractEntity $entity,
                                NotificationEventRepository $eventRepository,
                                TemplateRepository $templateRepository)
    {
        $this->entity = $entity;
        $this->eventRepository = $eventRepository;
        $this->templateRepository = $templateRepository;
    }

    public function getRootEntity()
    {
        return $this->entity;
    }

    public function getTemplateRepository()
    {
        return $this->templateRepository;
    }

    public function getEventRepository()
    {
        return $this->eventRepository;
    }
}
