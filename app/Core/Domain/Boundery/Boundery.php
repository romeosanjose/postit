<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 13/03/2019
 * Time: 1:49 PM
 */

namespace App\Core\Domain\Boundery;


abstract class Boundery
{
    /**
     * @param $data
     * @return string
     * default is array
     */
    public static function convertToJSON($data, $code, $message) {
        return json_encode(
            [
                "code" => $code,
                "message" => $message,
                "data" => $data
            ]
        );
    }
}