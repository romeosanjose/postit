<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 13/03/2019
 * Time: 1:17 PM
 */

namespace App\Core\Domain\Boundery\Decorator;

use App\Core\Domain\Entity\AbstractEntity;

abstract class Decorator
{
    abstract public function decorate(AbstractEntity $entity);

}