<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 13/03/2019
 * Time: 1:14 PM
 */

namespace App\Core\Domain\Boundery;

use App\Core\Domain\Boundery\Decorator\Decorator;

class Response extends Boundery
{

    private $code;

    private $data;

    private $message;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function transform(Decorator $decorator)
    {
        $this->setData($decorator->decorate($this->getData()));
        return $this;
    }

    public function toJSON()
    {
        return parent::toJSON($this->getData(),
            $this->getCode(),
            $this->getMessage()
        );
    }

}