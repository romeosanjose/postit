<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 13/03/2019
 * Time: 1:10 PM
 */

namespace App\Core\Domain\Boundery;

class Request extends Boundery
{
    private $data;

    private $code;

    private $message;

    public function __construct($data, $code = null, $message = null)
    {
        $this->data = $data;
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function toJSON()
    {
        return parent::convertToJSON($this->getData(),
            $this->getCode(),
            $this->getMessage()
        );
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return array_key_exists($key, $this->getData());
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->getData()[$key];
    }
}
