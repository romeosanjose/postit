<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 20/08/2019
 * Time: 6:12 PM
 */

namespace App\Core\Domain\Entity;


interface Identity
{
    public function getId();

    public function setId($id);

    public function getStatus();

    public function setStatus($status);

    public function isDeleted();

    public function setDeleted($deleted);
}