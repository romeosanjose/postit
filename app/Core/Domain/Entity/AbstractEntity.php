<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 13/02/2019
 * Time: 2:41 PM
 */

namespace App\Core\Domain\Entity;

use Doctrine\Common\Collections\Collection;

abstract class AbstractEntity
{
    /**
     * @return array
     */
    public function entityProperties()
    {
        return [
            'id',
            'uuid',
            'status',
            'deleted',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at'
        ];
    }

    private function returnIdOnly()
    {
        return [
            'created_by',
            'updated_by',
        ];
    }

    /**
     * @param $object
     */
    public function cast($object)
    {
        if (is_array($object) || is_object($object)) {
            foreach ($object as $key => $value) {
                $this->$key = $value;
            }
        }

        return $this;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function fromArray(array $attributes)
    {
        foreach ($attributes as $name => $value) {
            if (property_exists($this, $name)) {
                $methodName = $this->_getSetterName($name);
                if ($methodName) {
                    $this->setMethodValue($methodName, $value);
                } else {
                    $this->$name = $value;
                }
            }
        }

        return $this;
    }

    private function setMethodValue($methodName, $value)
    {
        if (is_array($value) || $value instanceof Collection) {
            foreach ($value as $v) {
                $this->{$methodName}($v);
            }
        } else {
            $this->{$methodName}($value);
        }
        return $this;
    }

    /**
     * @param $property
     * @return array
     */
    public function getValue($property)
    {
        if (!is_array($property)) {
            return $this->parseWithProperty($property);
        }
        $data = $this->parseWithMultipleProperties($property);

        return $data;
    }

    private function parseWithProperty($property)
    {
        $entityObject = $this->{$this->_getGettername($property)}();
        if ($entityObject instanceof AbstractEntity) {
            return $entityObject->getValue($entityObject->entityProperties());
        } elseif ($entityObject instanceof Collection) {
            return $this->collectIds($entityObject);
        } else {
            return $entityObject;
        }
    }

    private function parseWithMultipleProperties($properties)
    {
        $data = [];
        $properties = array_merge($properties, self::entityProperties());
        foreach ($properties as $p) {
            $entityObject = $this->{$this->_getGettername($p)}();
            if ($entityObject instanceof AbstractEntity) {
                if (in_array($p, $this->returnIdOnly())){
                    $data[$p] = $entityObject->getId();
                } else {
                    $data[$p] = $entityObject->getValue($entityObject->entityProperties());
                }
            } elseif ($entityObject instanceof Collection) {
                $data[$p] = $this->collectIds($entityObject);
            } else {
                $data[$p] = $entityObject;
            }
        }

        return $data;
    }

    private function collectIds(Collection $entityObjects)
    {
        $ids = [];
        foreach ($entityObjects as $e) {
            $ids[] = $e->getId();
        }

        return $ids;
    }

    protected function _getSettername($propertyName)
    {
        $prefixes = ['add', 'set'];
        return $this->getMethodName($propertyName, $prefixes);
    }

    protected function _getGettername($propertyName)
    {
        $prefixes = ['get', 'fetch', 'is'];
        return $this->getMethodName($propertyName, $prefixes);
    }

    protected function getMethodName($propertyName, $prefixes)
    {
        $camelledPropertyName = $this->toCammelCase($propertyName);
        foreach ($prefixes as $prefix) {
            $methodName = sprintf('%s%s', $prefix, $camelledPropertyName);

            if (method_exists($this, $methodName)) {
                return $methodName;
            }
        }

        return false;
    }

    private function toCammelCase($propertyName)
    {
        $propertyNameParticles = explode('_', $propertyName);
        $str = '';
        foreach ($propertyNameParticles as $p) {
            $str .= ucfirst(strtolower($p));
        }

        return $str;
    }


}
