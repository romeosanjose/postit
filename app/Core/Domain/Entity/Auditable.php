<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 15/03/2019
 * Time: 8:12 AM
 */

namespace App\Core\Domain\Entity;


interface Auditable
{
    public function getCreatedBy();

    public function setCreatedBy($createdBy);

    public function getUpdatedBy();

    public function setUpdatedBy($updatedBy);

    public function getCreatedAt();

    public function setCreatedAt($createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt);
}