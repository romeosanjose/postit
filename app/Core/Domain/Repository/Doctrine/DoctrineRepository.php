<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 14/06/2019
 * Time: 1:09 PM
 */

namespace App\Core\Domain\Repository\Doctrine;


use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Repository\CannotPersistException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use Ramsey\Uuid\Uuid;


class DoctrineRepository
{

    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->createTable();
    }

    public function startTransaction()
    {
        $this->em->getConnection()->beginTransaction();
        return $this;
    }

    public function commit()
    {
        $this->em->getConnection()->commit();
        return $this;
    }

    public function rollBack()
    {
        $this->em->getConnection()->rollBack();
        return $this;
    }

    public function lastId()
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('MAX(entity.id) AS max_id');
        $qb->from($this->entity(), 'entity');

        return $qb->getQuery()->getResult()[0]['max_id'];
    }

    public function find($id)
    {
        return  $this->entityManager->find($this->entity(), $id);
    }

    public function findByUuid($uuid)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('entity')
            ->from($this->entity(), 'entity')
            ->where('entity.uuid = :uuid')
            ->setParameter('uuid', $uuid);

        return $qb->getQuery()->getResult()[0];
    }

    public function uuid()
    {
        return Uuid::uuid4();
    }

    public function findActiveRecords()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('entity')
            ->from($this->entity(), 'entity')
            ->where('entity.deleted = 0');

        return $qb->getQuery()->execute();
    }

    public function findBy($propertyValue = [])
    {
        $qb = $this->entityManager->createQueryBuilder();
        $q = $qb->select('entity')
                ->from($this->entity(), 'entity');
        $counter = 0;
        foreach ($propertyValue as $property => $value) {

            if ($counter === 0) {
                $q = $q->where('entity.' . $property . ' = :column' . $counter);
                $q = $q->setParameter('column' . $counter, $value );
            } else {
                $q = $q->andWhere('entity.' . $property . ' = :column' . $counter);
                $q = $q->setParameter('column' . $counter, $value );
            }
            $counter++;
        }

        return $q->getQuery()->execute();
    }

    public function store(AbstractEntity $entity)
    {
        try{
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }catch(\Exception $e) {
            throw new CannotPersistException($e->getMessage());
        }
        return $entity;
    }

    public function update(AbstractEntity $entity, $entityName)
    {
        try{
            $entity = $this->entityManager->getReference($entityName, $entity->getId());
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }catch(\Exception $e) {
            throw new CannotPersistException($e->getMessage());
        }

        return $entity;
    }

    public function updateBy($updateValues = [], $where = []){

        $qb = $this->entityManager->createQueryBuilder();
        $q = $qb->update($this->entity(), 'entity');
        foreach($updateValues as $updateProperty => $updateValue) {
            $q = $q->set('entity.' . $updateProperty, ':' . $updateProperty);
            $q = $q->setParameter( $updateProperty, $updateValue);
        }

        $whereCounter = 0;
        foreach ($where as $property => $value) {
            if ($whereCounter === 0) {
                $q = $q->where('entity.' . $property . ' = :column' . $whereCounter);
                $q = $q->setParameter('column' . $whereCounter, $value );
            } else {
                $q = $q->andWhere('entity.' . $property . ' = :column' . $whereCounter);
                $q = $q->setParameter('column' . $whereCounter, $value );
            }
            $whereCounter++;
        }

        return $q->getQuery()->execute();

    }

    private function createTable()
    {
        $schemaTool = new SchemaTool($this->entityManager);
        $classes = $this->entityManager->getMetadataFactory()->getAllMetadata();
        try {
            $schemaTool->createSchema($classes);
        }catch(ToolsException $e) {
        }
    }


}
