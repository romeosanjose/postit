<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/08/2019
 * Time: 6:46 PM
 */

namespace App\Core\Domain\Repository;


use App\Core\Domain\Entity\AbstractEntity;

interface Repository
{
    public function entity();

    public function lastId();

    public function uuid();

    public function store(AbstractEntity $entity);

    public function find($id);

    public function findByUuid($uuid);

    public function findActiveRecords();

    public function findBy($propertyValue = []);

    public function updateBy($updateValues = [], $where = []);

    public function update(AbstractEntity $entity, $entityName);
}