<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 19/08/2019
 * Time: 4:27 PM
 */

namespace App\Core\Domain\Validator;

interface Validator
{
    public function validate();
}