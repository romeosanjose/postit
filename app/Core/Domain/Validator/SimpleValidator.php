<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 19/08/2019
 * Time: 4:40 PM
 */

namespace App\Core\Domain\Validator;

use SimpleValidator\Validator as SimpleValidatorEngine;

class SimpleValidator implements Validator
{

    private $data;

    private $rules;

    public function __construct($data, $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
    }


    public function validate()
    {
        $result = SimpleValidatorEngine::validate($this->data, $this->rules);
        if (!$result->isSuccess()) {
            throw new FormValidationException("Form Validation Failed", 500, null, $result->getErrors());
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param mixed $rules
     */
    public function setRules($rules): void
    {
        $this->rules = $rules;
    }



}