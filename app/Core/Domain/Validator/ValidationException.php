<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 19/08/2019
 * Time: 5:06 PM
 */

namespace App\Core\Domain\Validator;


use Throwable;

class ValidationException extends \RuntimeException
{
    protected $errors;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, $errors = [])
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors): void
    {
        $this->errors = $errors;
    }


}