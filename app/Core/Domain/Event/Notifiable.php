<?php


namespace App\Core\Domain\Event;



interface Notifiable
{
    public function getRootEntity();

    public function getEventName();


}
