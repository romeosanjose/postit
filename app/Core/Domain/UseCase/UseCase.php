<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/06/2019
 * Time: 8:32 AM
 */

namespace App\Core\Domain\UseCase;

use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Boundery\Decorator\PatientDecorator;
use App\Core\Domain\Boundery\Request;
use App\Core\Domain\Boundery\Response;
use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Repository\Repository;
use App\Core\Domain\Validator\SimpleValidator;
use App\Core\User\User;
use Ramsey\Uuid\Uuid;

class UseCase
{
    /**
     * @var Repository
     */
    protected $repository;

    protected $validationRules;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @param AbstractEntity $entity
     * @param User|null $createdBy
     * @param Decorator |null $decorator
     * @return mixed
     */
    public function addNewRecord(Request $request, AbstractEntity $entity, User $createdBy = null, Decorator $decorator = null, $validated = false)
    {
        $data = $request->getData();
        if (!$validated) {
            $this->validateUserData($data);
        }
        $entityObj = $entity->fromArray($data);
        $entityObj->setUuid(Uuid::uuid4());

        if (!is_null($createdBy)) {
            $entityObj = $this->setCreator($entityObj, $createdBy);
        }
        if (is_null($decorator)) {
            return $this->repository->store($entityObj);
        }

        return $this->storeDecorate($entityObj, $decorator);
    }

    /**
     * @param Decorator|null $decorator
     * @return Response
     */
    public function getAllActiveRecords(Decorator $decorator = null)
    {
        $entityObjs = $this->repository->findActiveRecords();
        $entityLists = [];
        if (is_null($decorator)) {
            return $entityObjs;
        }
        foreach ($entityObjs as $entity) {
            $entityLists[] = $decorator->decorate($entity);
        }

        return $this->response($entityLists);
    }

    /**
     * @param $id | $uuid
     * @param Decorator|null $decorator
     * @return Response
     */
    public function getRecord($id, Decorator $decorator = null)
    {
        $entityObject = $this->repository->find($id);
        if (!$entityObject) {
            $entityObject = $this->repository->findByUuid($id);
        }
        if (is_null($decorator) && empty($decorator)) {
            return $entityObject;
        }

        $entityData = $decorator->decorate($entityObject);
        return $this->response($entityData);
    }

    /**
     * @param Request $request
     * @param User|null $user
     * @return mixed
     */
    public function updateRecord(Request $request, User $user = null)
    {
        $data = $request->getData();
        $updateData = $data;
        if (!is_null($user)) {
            $updateData = $this->setEditor($updateData, $user);
        }
        unset($updateData['id']);
        return $this->repository->updateBy($updateData, ['id' => $data['id']]);
    }

    /**
     * @param AbstractEntity $entity
     * @param User $createdBy
     * @param string $decorator
     * @return mixed
     */
    protected function storeDecorate(AbstractEntity $entity, Decorator $decorator)
    {
        $entityObject = $this->repository->store($entity);
        $entityData = $decorator->decorate($entityObject);
        return $this->response($entityData);
    }

    /**
     * @param $data
     */
    protected function validateUserData($data)
    {
        (new SimpleValidator($data, $this->validationRules))->validate();
    }

    /**
     * @param $data
     * @param Response $currentRecord
     * @return array
     */
    protected function extractModifiedFields($data, Response $currentRecord)
    {
        $modified = [];
        foreach($data as $k => $v) {
            if ($data[$k] === $currentRecord->getData()[$k]) {
                $modified[$k] = $data[$k];
            }
        }

        return $modified;
    }

    protected function response($result, $code = 200)
    {
        $response = new Response($result);
        $response->setCode($code);

        return $response;
    }



    /**
     * @return mixed
     */
    public function getValidationRules()
    {
        return $this->validationRules;
    }

    /**
     * @param mixed $validationRules
     */
    public function setValidationRules($validationRules): void
    {
        $this->validationRules = $validationRules;
    }

    /**
     * @param AbstractEntity $entity
     * @param User $user
     * @return AbstractEntity
     */
    protected function setCreator(AbstractEntity $entity, User $user)
    {
        $entity->setCreatedBy($user);
        $entity->setCreatedAt(new \DateTime('now'));
        $entity->setUpdatedBy($user);
        $entity->setUpdatedAt(new \DateTime('now'));

        return $entity;
    }

    /**
     * @param $entityData
     * @param User $user
     * @return mixed
     */
    protected function setEditor($entity, User $user)
    {
        if ($entity instanceof AbstractEntity) {
            $entity->setUpdatedBy($user);
            $entity->setUpdatedAt(new \DateTime('now'));
            return $entity;
        }
        $entity['updated_by'] = $user;
        $entity['updated_at'] = new \DateTime('now');

        return $entity;
    }


}
