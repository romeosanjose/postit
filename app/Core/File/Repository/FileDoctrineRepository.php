<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/09/2019
 * Time: 4:59 PM
 */

namespace App\Core\File\Repository;


use App\Core\File\File;
use App\Core\Domain\Repository\Doctrine\DoctrineRepository;
use App\Core\File\Repository\FileRepository;

class FileDoctrineRepository extends DoctrineRepository implements FileRepository
{
    public function entity()
    {
        return File::class;
    }

}
