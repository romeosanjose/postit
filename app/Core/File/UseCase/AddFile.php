<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/09/2019
 * Time: 4:43 PM
 */

namespace App\Core\File\UseCase;


use App\Core\Domain\Boundery\Request;
use App\Core\Domain\UseCase\UseCase;
use App\Core\File\Decorator\FileDecorator;
use App\Core\File\File;
use App\Core\User\User;
use Illuminate\Support\Facades\Storage;

class AddFile extends UseCase
{
    protected $validationRules = [
        'name' => [
            'required',
            'max_length(50)'
        ],
        'path' => [
            'required',
        ],
        'extension' => [
            'required',
        ],
        'size' => [
            'required',
        ],
    ];

    public function addFile(Request $request, User $createdBy)
    {
        $file = $request->getData();
        $savedFile = $file->store(config('postit.storage.profile_picture'));
        if ($savedFile) {
            $fileData = [
                'size' => $file->getClientSize(),
                'extension' => $file->getClientOriginalExtension(),
                'path' => $savedFile,
                'name' => $file->getClientOriginalName(),
            ];

            return$this->addNewRecord(new Request($fileData), new File(), $createdBy, new FileDecorator());
        }
    }
}
