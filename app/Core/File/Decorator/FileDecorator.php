<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 19/09/2019
 * Time: 2:03 PM
 */

namespace App\Core\File\Decorator;


use App\Core\Domain\Boundery\Decorator\Decorator;
use App\Core\Domain\Entity\AbstractEntity;

class FileDecorator extends Decorator
{

    protected function properties()
    {
        // TODO: Implement properties() method.
    }

    public function decorate(AbstractEntity $entity)
    {
        return $entity->getValue($entity->entityProperties());
    }
}
