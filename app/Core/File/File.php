<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 15/03/2019
 * Time: 8:03 AM
 */
namespace App\Core\File;

use App\Core\Domain\Entity\AbstractEntity;
use App\Core\Domain\Entity\Auditable;
use App\Core\Domain\Entity\AuditableTrait;
use App\Core\Domain\Entity\Identity;
use App\Core\Domain\Entity\IdentityTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Files
 * @package App\Core\File
 * @ORM\Entity
 * @ORM\Table(name="files")
 */
class File extends AbstractEntity implements Identity, Auditable
{
    use AuditableTrait;
    use IdentityTrait;

    /**
     * @var mixed $name
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var mixed $path
     * @ORM\Column(type="string")
     */
    private $path;

    /**
     * @var mixed $description
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;
    /**
     * @var mixed $extension
     * @ORM\Column(type="string")
     */
    private $extension;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $size;

    function entityProperties()
    {
        return [
            'name',
            'path',
            'description',
            'extension',
            'size'
        ];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }



}
