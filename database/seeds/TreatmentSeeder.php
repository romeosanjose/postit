<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;


class TreatmentSeeder extends ManageListSeeder
{
    public function seedData()
    {
        return [
            'Oral Diagnosis' => 'TREATMENT',
            'Exo' => 'TREATMENT',
            'Flap' => 'TREATMENT',
            'Odontec' => 'TREATMENT',
            'Snp' => 'TREATMENT',
            'NSRP' => 'TREATMENT',
            'SRP' => 'TREATMENT',
            'Perio Surgery' => 'TREATMENT',
            'Perio Recall' => 'TREATMENT',
            'RCT' => 'TREATMENT',
            'Resto (Am)' => 'TREATMENT',
            'Resto (GIC)' => 'TREATMENT',
            'Inlay' => 'TREATMENT',
            'Onlay' => 'TREATMENT',
            'Crown' => 'TREATMENT',
            'Bridge' => 'TREATMENT',
            'OPF' => 'TREATMENT',
            'TFA' => 'TREATMENT',
            'PFS' => 'TREATMENT',
            'PRR' => 'TREATMENT',
            'SSC' => 'TREATMENT',
            'SOC' => 'TREATMENT',
            'Pulpo' => 'TREATMENT',
            'Pulpec' => 'TREATMENT',
            'Pulpo + SSC/SOC' => 'TREATMENT',
            'Space Maintainer' => 'TREATMENT',
            'RPD' => 'TREATMENT',
            'RPD-RPD' => 'TREATMENT',
            'SD-RPD' => 'TREATMENT',
            'SD' => 'TREATMENT',
            'CD' => 'TREATMENT',
            'CD Exam' => 'TREATMENT',
            'Ortho' => 'TREATMENT',
            'Special Case' => 'TREATMENT',
            'Others'  => 'TREATMENT'
        ];
    }

}
