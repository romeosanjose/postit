<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;


class ClinicalDeptSeeder extends ManageListSeeder
{
    public function seedData()
    {
        return [
            'OD' => 'CLINICAL_DEPT',
            'OM' => 'CLINICAL_DEPT',
            'OP' => 'CLINICAL_DEPT',
            'Prostho' => 'CLINICAL_DEPT',
            'Ortho' => 'CLINICAL_DEPT',
        ];
    }

}
