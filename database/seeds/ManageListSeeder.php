<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

abstract class ManageListSeeder extends Seeder
{

    abstract function seedData();

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->seedData() as $name => $module){
            $this->executeSeeding($name, $module);
        }
    }

    protected function executeSeeding($name, $module)
    {
        $manageListRepo = App::make(\App\Core\Setting\Repository\ManageListRepository::class);
        $manageList = new \App\Core\Setting\ManageList();
        $manageList->setUuid($manageListRepo->uuid());
        $manageList->setName($name);
        $manageList->setModule($module);
        $manageListRepo->store($manageList);
    }
}
