<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class YearLevelSeeder extends ManageListSeeder
{
    public function seedData()
    {
        return [
            '1st' => 'YEAR_LEVEL',
            '2nd' => 'YEAR_LEVEL',
            '3rd' => 'YEAR_LEVEL',
            '4th' => 'YEAR_LEVEL',
        ];
    }
}
