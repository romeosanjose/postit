<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personRepo = App::make(\App\Core\User\Repository\PersonRepository::class);
        $userRepo = App::make(\App\Core\User\Repository\UserRepository::class);

        $manageListRepo = App::make(\App\Core\Setting\Repository\ManageListRepository::class);
        $shiftColor = $manageListRepo->find(1);
        $clinicalDepartment = $manageListRepo->find(4);
        $yearLevel = $manageListRepo->find(9);
       

        $person = new \App\Core\User\Person();
        $person->setId(0);
        $person->setUuid($personRepo->uuid());
        $person->setFirstName('SuperFirst');
        $person->setLastName('SuperLast');
        $person->setShiftColor($shiftColor);
        $person->setClinicalDepartment($clinicalDepartment);
        $person->setYearLevel($yearLevel);

        $person = $personRepo->store($person);

        $user = new \App\Core\User\User();
        $user->setId(0);
        $user->setUuid($userRepo->uuid());
        $user->setEmail('admin@mail.com');
        $password = 'pass123';
        $user->setSalt(sha1($password));
        $user->setPassword(sha1($password . $user->getSalt()));
        $user->setApiKey(bin2hex(openssl_random_pseudo_bytes(64)));
        $user->setPerson($person);
        $user->setType('ADMIN');
//        $user->setSecurityQuestion($manageList);
//        $user->setSecurityAnswer('BESSY');
        $userRepo->store($user);
    }
}
