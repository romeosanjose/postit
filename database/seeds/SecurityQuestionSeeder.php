<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class SecurityQuestionSeeder extends ManageListSeeder
{
    public function seedData()
    {
        return [
            'What is your favorite color?' => 'SECURITY_QUESTION',
            'What is your mother\'s maiden name?' => 'SECURITY_QUESTION',
        ];
    }
}
