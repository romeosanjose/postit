<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(SecurityQuestionSeeder::class);
        $this->call(TreatmentSeeder::class);
        $this->call(ShiftColorSeeder::class);
        $this->call(ClinicalDeptSeeder::class);
        $this->call(YearLevelSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(NotificationEventTableSeeder::class);
    }
}
