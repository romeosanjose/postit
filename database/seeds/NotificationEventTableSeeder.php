<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class NotificationEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notificationEventRepo = App::make(\App\Core\Notification\Repository\NotificationEventRepository::class);

        foreach($this->events() as $eventDetails) {
            $event = new \App\Core\Notification\NotificationEvent();
            $event->setName($eventDetails['name']);
            $event->setEvent($eventDetails['event']);
            $event->setUuid($notificationEventRepo->uuid());
            $notificationEventRepo->store($event);
        }

    }

    private function events()
    {
        return [
            ['name' => 'User Created', 'event' => \App\Core\User\Events\UserCreated::class],
        ];
    }


}
