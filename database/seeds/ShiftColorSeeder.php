<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class ShiftColorSeeder extends ManageListSeeder
{
    public function seedData()
    {
        return [
            'Blue' => 'SHIFT_COLOR',
            'Green' => 'SHIFT_COLOR',
            'Red' => 'SHIFT_COLOR',
        ];
    }
}
