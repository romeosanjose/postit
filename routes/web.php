<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


//Home
$router->get('/', 'HomeController@showLandingPage');
//Login
$router->post('/login', 'LoginController@loginAction');

$router->group(['middleware' => ['auth-api','allow-user']], function () use ($router){
    // -----------------  User Requests ------------------- //
    $router->get('/user', 'UserController@getAllAction');
    $router->get('/user/{id}', 'UserController@getUser');
    $router->post('/user', 'UserController@storeAction');
    $router->get('/user/password/generate', 'PasswordController@generatePassword');
    $router->patch('/user/{id}', 'UserController@updateAction');
    $router->post('/user/{id}/unlock', 'UserController@unlockAction');
    $router->patch('/user/{id}/follow', 'UserController@followAction');
    $router->patch('/user/{id}/unfollow', 'UserController@unFollowAction');
    // -----------------  Person Requests ------------------- //
    $router->post('/person', 'PersonController@storeAction');
    // -----------------  Patient Requests ------------------- //
    $router->post('/patient', 'PatientController@storeAction');
    $router->get('/patient', 'PatientController@getAllAction');
    $router->get('/patient/{id}', 'PatientController@getPatientAction');
    // -----------------  Post Requests ------------------- //
    $router->get('/post', 'PostController@getAllPostAction');
    $router->get('/post/treatment/{treatment}', 'PostController@getAllPostByTreatmentAction');
    $router->get('/post/{id}', 'PostController@getPostAction');
    $router->post('/post', 'PostController@storeAction');
    $router->patch('/post/{id}', 'PostController@acceptPostAction');
    $router->put('/post/{id}', 'PostController@updatePost');
    // -----------------  Event Requests ------------------- //
    $router->get('/event', 'EventController@getAllAction');
    $router->get('/event/{id}', 'EventController@getEvent');
    // -----------------  Template Requests ------------------- //
    $router->post('/template', 'TemplateController@addTemplateAction');
    $router->get('/template', 'TemplateController@getTemplates');
    $router->get('/template/{id}', 'TemplateController@getTemplate');
    $router->patch('/template/{id}', 'TemplateController@updateTemplate');
    // -----------------  Event Requests ------------------- //
    $router->post('/event', 'EventController@storeAction');
    // -----------------  File Requests ------------------- //
    $router->post('/file', 'FileController@storeAction');
    $router->get('/file/{id}','FileController@getAction');
    // -----------------  Managelists Requests ------------------- //
    $router->get('/managelist/{module}','ManageListController@getAllAction');
});

$router->post('/password/change/{uuid}','PasswordController@changePassword');
$router->post('/password/forgot', 'PasswordController@sendPasswordLink');

