<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 17/08/2019
 * Time: 6:35 AM
 */


return [
    'repositories' => [
        \App\Core\User\Repository\UserRepository::class => \App\Core\User\Repository\UserDoctrineRepository::class,
        \App\Core\User\Repository\PersonRepository::class => \App\Core\User\Repository\PersonDoctrineRepository::class,
        \App\Core\Post\Repository\PostRepository::class => \App\Core\Post\Repository\PostDoctrineRepository::class,
        \App\Core\Post\Repository\PatientRepository::class => \App\Core\Post\Repository\PatientDoctrineRepository::class,
        \App\Core\File\Repository\FileRepository::class => \App\Core\File\Repository\FileDoctrineRepository::class,
        \App\Core\Notification\Repository\TemplateRepository::class => \App\Core\Notification\Repository\TemplateDoctrineRepository::class,
        \App\Core\Notification\Repository\NotificationEventRepository::class => \App\Core\Notification\Repository\NotificationEventDoctrineRepository::class,
        \App\Core\Event\Repository\EventRepository::class => \App\Core\Event\Repository\EventDoctrineRepository::class,
        \App\Core\Setting\Repository\SettingRepository::class => \App\Core\Setting\Repository\SettingDoctrineRepository::class,
        \App\Core\Setting\Repository\ManageListRepository::class => \App\Core\Setting\Repository\ManageListDoctrineRepository::class,

    ],

    'permissions' => [
        'ADMIN' => [
            'user' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'person' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'patient' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'post' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'event' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'template' => ['POST','GET', 'PATCH', 'PUT', 'DELETE'],
            'file' => ['POST','GET', 'PATCH', 'PUT'],
            'managelist' => ['POST','GET', 'PATCH', 'PUT']
        ],
        'PUBLISHER' => [
            'user' => ['GET'],
            'person' => ['GET'],
            'patient' => ['POST','GET', 'PATCH', 'PUT'],
            'post' => ['POST','GET', 'PATCH', 'PUT'],
            'file' => ['POST','GET', 'PATCH', 'PUT'],
            'managelist' => ['GET']
        ]
    ],

    'own_permission' => [
        'user',
        'person',
    ],

    'storage' => [
        'profile_picture' => 'profile_picture'
    ],

];
