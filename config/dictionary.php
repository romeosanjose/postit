<?php

return [
	// messages for login
    'reached_maximum_attempt' => 'Incorrect Access. You have reached the maximum attempt to login. We have temporarily locked your account. 
        Please contact your administrator to unlock your account',
    'incorrect_access' => 'email or password is incorrect',
    'user_locked' => 'User is locked. Please contact your administration to proceed on your account.',
    

];