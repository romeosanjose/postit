<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 11/03/2019
 * Time: 12:46 PM
 */

class UserTest extends TestCase
{
   public function testSuccessfulLogin()
   {
       $user = new \App\Core\Domain\Entity\User();
       $user->setUsername("superman");
       $user->setPassword('password123');
       $user->setSalt(sha1('password123'));
       $user->setHashPassword(sha1('password123' . $user->getSalt()));
       $result = $user->authenticate(
           "superman",
           "password123"
       );

       $this->assertTrue($result);
   }

   public function testInvalidLogin()
   {
       $user = new \App\Core\Domain\Entity\User();
       $user->setUsername("superman");
       $user->setPassword('password123');
       $user->setSalt(sha1('password123'));
       $user->setHashPassword(sha1('password123' . $user->getSalt()));
       $result = $user->authenticate(
           "superman",
           "password12345"
       );

       $this->assertFalse($result);
   }
}