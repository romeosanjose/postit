<?php
/**
 * Created by PhpStorm.
 * User: rsanjose
 * Date: 14/03/2019
 * Time: 1:58 PM
 */

class DecoratorTest extends TestCase
{
    public $objectNames = [
        "User"
    ];

    public function testTransformObjectToStructure()
    {
        foreach($this->objectNames as $objName) {
            $entityNameSpace = "App\\Core\\Domain\\Entity\\$objName";
            $entity = new  $entityNameSpace;
            $entity->setId(1);

            $decoratorNameSpace = "App\\Core\\Domain\\Boundery\\Decorator\\$objName"."Decorator";
            $decorator = new $decoratorNameSpace($entity);
            $result = $decorator->decorate($entity);
            $this->assertTrue(is_array($result));
        }
    }
}